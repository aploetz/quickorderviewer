﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using QuickOrderCustomerViewer.Models;
using QuickOrderCustomerViewer.Services;

namespace QuickOrderCustomerViewer.Controllers
{
    [RequireHttps]
    public class SupplierCustomerController : Controller
    {
        SupplierCustomerRepository custDAO;
        SupplierCustomerService custSvc;

        private void initServices()
        {
            if (custDAO == null)
            {
                custDAO = new SupplierCustomerRepository();
            }

            if (custSvc == null)
            {
                custSvc = new SupplierCustomerService(custDAO);
            }
        }

        // GET: AllCustomers
        public ActionResult Index()
        {
            initServices();

            var allCustomers = custSvc.getAllCustomers();

            return View("Index", allCustomers);
        }

        // GET: A single customer by customer number and supplier id
        public ActionResult CustomerDetails(string customerno, Guid supplierid)
        {
            initServices();

            var customer = custSvc.getCustomer(customerno, supplierid);
            
            return View("CustomerDetails", customer);
        }


    }
}