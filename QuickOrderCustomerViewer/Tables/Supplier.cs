﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuickOrderCustomerViewer.Tables
{
    public class Supplier
    {
        public Guid supplierid { get; set; }
        public string specialdescriptions { get; set; }
        public string specialhierarchy { get; set; }
        public string suppliername { get; set; }
    }
}
