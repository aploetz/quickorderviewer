﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuickOrderCustomerViewer.Tables
{
    public class SupplierCustomer
    {
        public String customerNo { get; set; }
        public String customerName { get; set; }
        public String branchID { get; set; }
        public String custAuthCode { get; set; }
        public String custAuthDate { get; set; }
        public String subsidizationEndDate { get; set; }
        public Guid supplierID { get; set; }
        public string supplierName { get; set; }
        public String paidUsers { get; set; }
        public List<String> approvedBranches { get; set; }
    }
}