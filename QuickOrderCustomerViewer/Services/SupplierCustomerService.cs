﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using QuickOrderCustomerViewer.Models;
using QuickOrderCustomerViewer.Tables;

namespace QuickOrderCustomerViewer.Services
{
    public class SupplierCustomerService
    {
        private SupplierCustomerRepository custDAO;

        public SupplierCustomerService()
        {
            if (custDAO == null)
            {
                custDAO = new SupplierCustomerRepository();
            }
        }

        public SupplierCustomerService(SupplierCustomerRepository dao)
        {
            custDAO = dao;
        }

        public List<SupplierCustomer> getAllCustomers()
        {
            Dictionary<Guid, string> supplierMap = buildSupplierMap();
            List<SupplierCustomer> customers = custDAO.getAllCustomers();

            //set supplier name
            foreach(SupplierCustomer cust in customers)
            {
                if (supplierMap.ContainsKey(cust.supplierID))
                {
                    cust.supplierName = supplierMap[cust.supplierID];
                }
            }

            return customers;
        }

        public SupplierCustomer getCustomer(string customerno, Guid supplierid)
        {
            Dictionary<Guid, string> supplierMap = buildSupplierMap(); 
            SupplierCustomer customer = custDAO.getCustomer(customerno, supplierid);

            //set supplier name
            if (supplierMap.ContainsKey(customer.supplierID))
            {
                customer.supplierName = supplierMap[customer.supplierID];
            }

            //get AccuLynx customer info

            return customer;
        }

        public List<Supplier> getAllSuppliers()
        {
            List<Supplier> suppliers = custDAO.getAllSuppliers();

            return suppliers;
        }

        private Dictionary<Guid,string> buildSupplierMap()
        {
            Dictionary<Guid, string> supplierMap = new Dictionary<Guid, string>();
            //get suppliers
            List<Supplier> allSuppliers = getAllSuppliers();
            //build supplier map
            foreach (Supplier supplier in allSuppliers)
            {
                supplierMap.Add(supplier.supplierid, supplier.suppliername);
            }

            return supplierMap;
        }

        private string getAccountNumber(string customerno)
        {
            //SELECT pca.AccountNumber, c.LocationNumber
            //FROM companysettings cs
            //INNER JOIN companies c ON c.CompanyID = cs.CompanyID
            //INNER JOIN ParentCompanyAccounts pca ON pca.ParentCompanyAccountID = c.ParentCompanyAccountID
            //WHERE cs.ValueString = 'customerno';

            return string.Empty;
        }
    }
}