﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Cassandra;

namespace QuickOrderCustomerViewer.Models
{
    public class CassandraDAO
    {
        private Cluster cluster;

        public CassandraDAO()
        {

        }

        public ISession connect()
        {
            string[] nodes = Properties.Settings.Default.cassDBServer.Split(',');

            QueryOptions queryOptions = new QueryOptions().SetConsistencyLevel(ConsistencyLevel.One);

            cluster = Cluster.Builder()
                .WithCredentials(Properties.Settings.Default.cassUser, Properties.Settings.Default.cassPassword)
                .WithQueryOptions(queryOptions)
                .AddContactPoints(nodes).Build();
            ISession session = cluster.Connect();

            return session;
        }

        //destructor
        ~CassandraDAO()
        {
            cluster.Shutdown();
        }
    }
}