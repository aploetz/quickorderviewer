﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Cassandra;
using Cassandra.Mapping;

using QuickOrderCustomerViewer.Tables;

namespace QuickOrderCustomerViewer.Models
{
    public class SupplierCustomerRepository : CassandraDAO
    {
        private ISession session;
        private IMapper mapper;

        private string strCQL1;
        private string strCQL2;
        private string strCQL3;
        private string strCQL4;

        public SupplierCustomerRepository()
        {
            if (session == null)
            {
                session = connect();
                mapper = new Mapper(session);
            }

            //prepare CQL string statements
            strCQL1 = "SELECT customerno, supplierid, branchid, approvedbranches, "
                + "custauthcode, custauthdate, customername, paidusers, subsidizationenddate "
                + "FROM customers.customermaster "
                + "WHERE customerNo=? AND supplierid=? ";

            strCQL2 = "SELECT customerno, supplierid, branchid, approvedbranches, "
                + "custauthcode, custauthdate, customername, paidusers, subsidizationenddate "
                + "FROM customers.customermaster ";

            strCQL3 = "SELECT supplierid, specialdescriptions, specialhierarchy, suppliername "
                + "FROM customers.suppliers "
                + "WHERE supplierid=? ";

            strCQL4 = "SELECT supplierid, specialdescriptions, specialhierarchy, suppliername "
                + "FROM customers.suppliers ";
            //end prepare statements
        }

        //destructor
        ~SupplierCustomerRepository()
        {
            if (session != null)
            {
                session.Dispose();
            }
        }

        public SupplierCustomer getCustomer(string customerno, Guid supplierid)
        {
            return mapper.First<SupplierCustomer>(strCQL1, customerno, supplierid);
        }

        public List<SupplierCustomer> getAllCustomers()
        {
            IEnumerable<SupplierCustomer> customers = mapper.Fetch<SupplierCustomer>(strCQL2);
            return customers.ToList();
        }

        public Supplier getSupplier(Guid supplierid)
        {
            return mapper.First<Supplier>(strCQL3);
        }

        public List<Supplier> getAllSuppliers()
        {
            IEnumerable<Supplier> suppliers = mapper.Fetch<Supplier>(strCQL4);
            return suppliers.ToList();
        }
    }
}