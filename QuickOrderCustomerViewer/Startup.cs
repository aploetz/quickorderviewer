﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(QuickOrderCustomerViewer.Startup))]
namespace QuickOrderCustomerViewer
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
